Полное описание лабораторной смотри в  Описание лабораторной.odt
```bash
packer validate ./ubuntu18.json
```
Теперь запустите процесс сборки образа:
```bash
packer build ./ubuntu18.json
```
Проверьте что образ создался.  В папке  output должен появиться образ ubuntu-18.04.application.box

Проверка образа - создание ВМ
Создать образ можно с помощью vagrant
```bash
vagrant box add ubuntu18application output/ubuntu-18.04.application.box
```
Вывод в консоли
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'ubuntu18application' (v0) for provider:
box: Unpacking necessary files from: file://F:/Teaching/devops/labs/packer/packer_templates/Ubuntu-18.04/output/ubuntu-18.04.application.box
box:
==> box: Successfully added box 'ubuntu18application' (v0) for 'virtualbox'!

Также установим плагин для установки virtual box дополнений
```bash
vagrant plugin install vagrant-vbguest --plugin-clean-sources --plugin-source https://rubygems.org
```

```bash
vagrant init ubuntu18application
```

Вывод в консоли
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.

Появился  Vagrantfile
Добавим в него настройки подключения (не безопасно, в пром среде лучше использовать ssh_key)

Запустим ВМ
```bash
vagrant up
```
Подключимся к виртуальной машине  и проверим наличие установленных приложений.
```bash
vagrant ssh application
```
Внутри vm запустим redis и приложения
```bash
sudo service redis-server start
python3 ddd2023/compose/app/app.py
```

Добавим проброс портов в virtual box и проверим работу приложения
Можно настроить vagrant для автоматического запуска приложения.
https://developer.hashicorp.com/vagrant/docs/provisioning/shell

$script = <<-SCRIPT
echo I am provisioning...
sudo service redis-server start
python3 ddd2023/compose/app/app.py
SCRIPT

application.vm.provision "shell", inline: $script

А также автоматически прописать проброс портов
application.vm.network "forwarded_port", guest: 8080, host: 8083, host_ip: "127.0.0.1"

Проверим что приложение запустилось:
![img.png](img.png)

Завершим работу и удалим машину
```bash
vagrant destroy
```

* чтобы использовать терминал в intellij
добавить /usr/bin/env -- flatpak-spawn --host bash в настройки

